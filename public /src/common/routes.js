import React from "react";
import { Route } from "react-router";

import Login from "src/common/components/Login";
import Dashboard from "src/common/components/Dashboard";
import Layout from "src/common/components/Layout";

export default (
  <Route components={Layout}>
    <Route path="/" component={Dashboard} />
    <Route path="login" component={Login} />
    <Route path="dashboard" component={Dashboard} />
  </Route>
);
