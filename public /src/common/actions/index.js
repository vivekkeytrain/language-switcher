import request from "superagent";

/*  Action types    */
export const CHANGE_ACTION = "CHANGE_ACTION";

/*  Async action types  */
export const FETCH_SIGNIN_REQUEST = "FETCH_SIGNIN_REQUEST";
export const FETCH_SIGNIN_FAILURE = "FETCH_SIGNIN_FAILURE";
export const FETCH_SIGNIN_SUCCESS = "FETCH_SIGNIN_SUCCESS";

export const FETCH_SIGNUP_REQUEST = "FETCH_SIGNUP_REQUEST";
export const FETCH_SIGNUP_FAILURE = "FETCH_SIGNUP_FAILURE";
export const FETCH_SIGNUP_SUCCESS = "FETCH_SIGNUP_SUCCESS";

export const FETCH_PASSWORD_RESET_REQUEST = "FETCH_PASSWORD_RESET_REQUEST";
export const FETCH_PASSWORD_RESET_FAILURE = "FETCH_PASSWORD_RESET_FAILURE";
export const FETCH_PASSWORD_RESET_SUCCESS = "FETCH_PASSWORD_RESET_SUCCESS";

/*  Actions     */
export function changeAction(action) {
  return { type: CHANGE_ACTION, action };
}

/*  Async actions   */
function fetchSigninRequest(username, password) {
  return { type: FETCH_SIGNIN_REQUEST, username, password };
}

function fetchSigninFailure(ex) {
  return { type: FETCH_SIGNIN_FAILURE, ex };
}

function fetchSigninSuccess(user_data, messages) {
  return { type: FETCH_SIGNIN_SUCCESS, user_data, messages };
}

export function do_signin(username, password) {
  console.log("do_signin", username, password);

  return dispatch => {
    dispatch(fetchSigninRequest(username, password));

    return request
      .post("/api/login")
      .send({ username, password })
      .then(response => response.body, ex => dispatch(fetchSigninFailure(ex)))
      .then(json => dispatch(fetchSigninSuccess(json[0][0], json[1] || [])))
      .catch(ex => dispatch(fetchSigninFailure(ex)));
  };
}
