import React, { Component, PropTypes } from "react";
import { Link } from "react-router";

class Reset extends Component {
  constructor(props) {
    // console.log("Reset constructor", props);
    super(props);

    this.onChangePassword = this.onChangePassword.bind(this);
  }

  componentDidMount() {
    this.props.fetchUserData(this.props.params.uid);
  }

  onChangePassword(e) {
    e.preventDefault();

    this.props.onChangePassword();
  }

  render() {
    console.log("render reset");
    // console.log(this.props);
    return (
      <div className="form-box signin" id="login-box">
        <div className="widget-box visible">
          <div className="header bg-navy">
            {this.props.site_type == 2
              ? <img src="/img/ACT_CR101_logo_rev_lg.png" width="330" alt="" />
              : <img
                  src="/img/act-keytrain_rev.png"
                  alt=""
                  width="330"
                  className="img-responsive"
                />}
          </div>
          <div className="bg-gray" style={{ padding: "20px 10px" }}>
            <h4 className="red">Reset Your Password</h4>
          </div>
          <form onSubmit={this.onChangePassword} role="form" noValidate>
            <div className="bg-gray body">
              <div className="controls">
                <label htmlFor="password1" className="control-label">
                  New Password
                </label>
                <input
                  type="password"
                  name="password1"
                  id="password1"
                  className="form-control"
                />
                <label htmlFor="password2" className="control-label">
                  New Password Again
                </label>
                <input
                  type="password"
                  name="password2"
                  id="password2"
                  className="form-control"
                />
              </div>
            </div>
          </form>
          <div className="footer">
            <button
              type="s4ubmit"
              id="forgot-submit-button"
              className="btn btn-primary btn-block btn-lg"
            >
              Submit
            </button>
            <div>
              <span className="for-signin inline">
                <Link to="/">
                  Back to login
                </Link>
              </span>
              <br />
            </div>
          </div>
        </div>
        <br /><br />
      </div>
    );
  }
}

Reset.propTypes = {
  site_type: PropTypes.number,
  fetchUserData: PropTypes.func
};

export default Reset;
