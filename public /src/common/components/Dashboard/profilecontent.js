import React, { PropTypes } from "react";

const Profilecontent = ({ orgzname }) => (
	<div id="profile-contener" >
		<div className="row pad-tb-s70 brand-white-bg gray-dark" >
			<div className="container">
				<div className="row main">
					<div className="col-md-12" >
						<div className="main-login main-center">
							<h2>Update User Information & Organization Access</h2>
							<form className="" method="post" action="#">
								<div className="row" >
									<div className="col-md-8 form-user-left" >
										<h4>User Information</h4>
										<div className="form-group">
											<label for="s_name" className="cols-sm-2 control-label">School Name</label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-institution fa" aria-hidden="true"></i></span>
													<input type="text" className="form-control" name="s_name" id="name"  placeholder="Vivek Demo"/>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="l_name" className="cols-sm-2 control-label">Full Name <span className="required" >*</span></label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
													<input type="text" className="form-control" name="name" id="l_name"  placeholder="Last Name"/>
													<input type="text" className="form-control" name="name" id="m_name" placeholder="Middle Name"/>
													<input type="text" className="form-control" name="name" id="f_name" placeholder="First Name"/>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="user_name" className="cols-sm-2 control-label">User Name <span className="required" >*</span></label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-address-card-o fa" aria-hidden="true"></i></span>
													<input type="text" className="form-control" name="name" id="user_name"  placeholder="vivek-admin"/>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="pass" className="cols-sm-2 control-label">Password <span className="required" >*</span></label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-lock fa" aria-hidden="true"></i></span>
													<input type="password" className="form-control" name="name" id="pass"  placeholder="Password"/>
													<input type="password" className="form-control" name="name" id="name"  placeholder="Varify Password"/>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="name" className="cols-sm-2 control-label">Phone/Email <span className="required" >*</span></label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-phone fa" aria-hidden="true"></i></span>
													<input type="text" className="form-control" name="name" id="name"  placeholder="+1 123 456 7896"/>
													<input type="text" className="form-control" name="name" id="name"  placeholder="Vivek.Munukuntla@act.org"/>
												</div>
											</div>
										</div>
										<div className="form-group float-md-left1">
											<label for="address" className="cols-sm-2 control-label">Address 1</label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-map-marker fa" aria-hidden="true"></i></span>
													<textarea rows="5" cols="23" id="address" placeholder="Address 1" ></textarea>
													<textarea rows="5" cols="23" placeholder="Address 2" ></textarea>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="city" className="cols-sm-2 control-label">City/State/Zipcode</label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa fa-map fa" aria-hidden="true"></i></span>
													<input type="text" className="form-control" name="name" id="city"  placeholder="City"/>
													<input type="text" className="form-control" name="name" id=""  placeholder="State"/>
													<input type="text" className="form-control" name="name" id=""  placeholder="Zipcode"/>
												</div>
											</div>
										</div>
										
										<div className="form-group">
											<label for="status" className="cols-sm-2 control-label">Status</label>
											<div className="cols-sm-10">
												<div className="input-group status">
													<span className="input-group-addon"><i className="fa-calendar-check-o fa" aria-hidden="true"></i></span>
													<div className="col-md-2" > 
														<label><input type="checkbox" className="form-control" name="name" id="status" /> Active</label>
													</div>
													<div className="col-md-3" > 
														<label><input type="radio" className="form-control" name="radio" id="admin1" /> Administrator</label>
													</div>
													<div className="col-md-4" > 
														<label><input type="radio" className="form-control" name="radio" id="admin2" /> Administrator (Reports only)</label>
													</div>
													<div className="col-md-3" > 
														<label><input type="radio" className="form-control" name="radio" id="admin3" /> Instructor</label>
													</div>
												</div>
											</div>
										</div>
										<div className="form-group">
											<label for="name" className="cols-sm-2 control-label">Notes</label>
											<div className="cols-sm-10">
												<div className="input-group">
													<span className="input-group-addon"><i className="fa-file-text fa" aria-hidden="true"></i></span>
													<textarea rows="5" cols="23" ></textarea>
												</div>
											</div>
										</div>
									</div>
									<div className="col-md-4 form-user-right" >
										<h4>Organization Access</h4>
										<p>This user has access to the 13 organizations below. <a href="#" >Manage</a></p>
										<div className="" >
											<ul>
												<li><input type="checkbox" name="" checked="true" /> <strong>Vivek Demo (default)</strong> </li>
												<li><input type="checkbox" name="" checked="true" /> ACT Career Curriculum </li>
												<li><input type="checkbox" name=""  /> ACT Curriculum Review </li>
												<li><input type="checkbox" name="" checked="true" /> ACT Curriculum Team </li>
												<li><input type="checkbox" name="" checked="true" /> ACT Demo Accounts </li>
												<li><input type="checkbox" name="" checked="true" /> ACT Employees Demo </li>
												<li><input type="checkbox" name="" checked="true" /> Anand Demo </li>
												<li><input type="checkbox" name="" checked="true" /> Coptix </li>
												<li><input type="checkbox" name="" checked="true" /> Coptix - Limited </li>
												<li><input type="checkbox" name="" checked="true" /> HTML5 Mobile Preview </li>
												<li><input type="checkbox" name="" checked="true" /> KeyTrain Individual Users I-Store </li>
												<li><input type="checkbox" name="" checked="true" /> Little Lebowski Urban Achievers </li>
												<li><input type="checkbox" name="" checked="true" /> Total Quality Management </li>
											</ul>
										</div>
									</div>
								</div>
									<div className="row" >
										<div className="col-md-2">
											<a href="#" target="_blank" type="button" id="button" className="btn btn-primary btn-lg btn-block login-button">Save</a>
										</div>
										<div className="col-md-3">
											<a href="#" target="_blank" type="button" id="button" className="btn btn-primary btn-lg btn-block login-button">Save & New</a>
										</div>
										<div className="col-md-3">
											<a href="#" target="_blank" type="button" id="button" className="btn btn-primary btn-lg btn-block login-button">Save & Close</a>
										</div>
										<div className="col-md-2">
											<a href="#" target="_blank" type="button" id="button" className="btn btn-primary btn-lg btn-block login-button">Reset</a>
										</div>
										<div className="col-md-2">
											<a href="#" target="_blank" type="button" id="button" className="btn btn-primary btn-lg btn-block login-button">Cancel</a>
										</div>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
);

Profilecontent.propTypes = { orgzname: PropTypes.string };

export default Profilecontent;
