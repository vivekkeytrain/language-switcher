import React, { PropTypes } from "react";

const Dashboardheader = ({ propUserName, flagName, setLangEng, setLangEsp }) => (
	<div>
		<div className="row gray-darker-bg pad-tb-s5" id="upper-header" >
			<div className="container" >
				<div className="pull-right brand-white" >
					<nav className="navbar">
						<ul className="nav navbar-nav font-size-smaller">
							<li className="pad-tb-s5 dropdown-toggle-contener" >
								<a className="brand-white dropdown-toggle" data-toggle="dropdown" >
									<span className={flagName} ><img src="/img/US.png" /></span>
									<span className="caret"></span>
								</a>
								<ul className="dropdown-menu pad-s30">
									<li><a className="gray-dark flag-english font-size-smaller" onClick={flagName = "flagEng"} ><img src="/img/US.png" />  English</a></li>
									<li><a className="gray-dark flag-espan font-size-smaller" onClick={flagName = "flagEsp"}><img src="/img/ES.png" />  Español</a></li>
								</ul>
							</li>
							<li className="pad-tb-s5 dropdown-toggle-contener" >
								<a className="brand-white dropdown-toggle" data-toggle="dropdown" >
									<i className="fa fa-user" ></i> Welcome {propUserName}
									<span className="caret"></span>
								</a>
								<ul className="dropdown-menu pad-s30">
									<li><a id="show_profile_link" className="gray-dark font-size-smaller" ><i className="fa fa-user" ></i> Edit Profile</a></li>
									<li>
										<a>
											<p className="gray-dark font-size-smaller">
												<b>Account type:</b> Administrator
												<b>Username:</b> vivek-admin
											</p>
										</a>
									</li>
								  <li><a id="show_profile_link" className="gray-dark font-size-smaller" >Edit My Account</a></li>
								</ul>
							</li>
							<li className="pad-tb-s5" ><a className="brand-white"><i className="fa fa-life-ring" ></i> Support</a></li>
							<li className="pad-tb-s5" ><a className="brand-white"><i className="fa fa-lock" ></i> Logout</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div className="row" id="header" >
			<div className="container" >
				<div className="row" >
					<div className="col-sm-3 pad-tb-s20" >
						<img
						  src="/img/act-keytrain_rev.png"
						  width="200"
						  alt="ACT Career"
						  className="img-responsive home-link"
						/>
					</div>
					<div className="col-sm-6" >
						 <nav className="navbar">
							<ul className="nav navbar-nav">
								<li className="active text-uppercase font-bold" ><a href="#" className="gray-lighter home-link" >Home</a></li>
								<li className="text-uppercase font-bold" >
									<a href="#" className="gray-lighter dropdown-toggle" data-toggle="dropdown" >
									Setup <span className="caret"></span></a>
									<ul className="dropdown-menu pad-s30 mega-menu">
										<li>
											<div className="col-md-3 pad-lr-s10" >
												<ul className="">
													<li className="sub-menu-heading" >This Organization</li>
													<li><a href="#" className="gray-dark font-size-smaller" >About Organization</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Organization Options</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Active Courses</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Organization Assignments</a></li>
												</ul>
											</div>
											<div className="col-md-3 pad-lr-s10" >
												<ul className="">
													<li className="sub-menu-heading" >This Class</li>
													<li><a href="#" className="gray-dark font-size-smaller" >No class selected</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Add New Class</a></li>
												</ul>
											</div>
											<div className="col-md-3 pad-lr-s10" >
												<ul className="">
													<li className="sub-menu-heading" >Instructors*</li>
													<li><a href="#" className="gray-dark font-size-smaller" >Add New Instructor</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Browse Instructors</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Delete Instructor</a></li>
												</ul>
											</div>
											<div className="col-md-3 pad-lr-s10" >
												<ul className="">
													<li className="sub-menu-heading" >Students</li>
													<li><a href="#" className="gray-dark font-size-smaller" >Add New Student</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Browse Students</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Delete Student</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Groups</a></li>
													<li><a href="#" className="gray-dark font-size-smaller" >Student Assignments</a></li>
												</ul>
											</div>
										</li>
									</ul>
									
								</li>
								<li className="text-uppercase font-bold" ><a href="#" className="gray-lighter" >Teach</a>
								</li>
								<li className="text-uppercase font-bold" ><a href="#" className="gray-lighter" >Calender</a></li>
								<li className="text-uppercase font-bold" ><a href="#" className="gray-lighter" >Reports</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
);

Dashboardheader.propTypes = { 
	propUserName: PropTypes.string.isRequired,
	flagName: PropTypes.string.isRequired,
	setLangEng: PropTypes.func,
	setLangEsp: PropTypes.func,
};
Dashboardheader.setLangEng = function(){
	console.log('test1');
	flagName = 'test1';
}
function setLangEsp(){
	console.log('test2');
	Dashboardheader.flagName = 'test2';
}
Dashboardheader.defaultProps = {
   propUserName: 'Vivek Munukuntla',
      flagName: "flag-english",
}
export default Dashboardheader;
