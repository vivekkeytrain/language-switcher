import React, { PropTypes } from "react";

const SelfSignupTop = ({ selfSignup, action, changeAction }) => {
  console.log("SelfSignupTop", selfSignup, action);
  if (selfSignup) {
	  return (
		<div>
		  <p>
			User dashboard?{" "}
			<a href="#" onClick={e => changeAction("signin", e)}>Sign in</a>
		  </p>
		</div>
	  );
  } else {
    return <span/>;
  }
};

SelfSignupTop.propTypes = {
  selfSignup: PropTypes.object,
  action: PropTypes.string.isRequired,
  changeAction: PropTypes.func.isRequired
};

export default SelfSignupTop;
