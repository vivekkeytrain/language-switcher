import React, { PropTypes } from "react";

const Dashboardcontent = ({ orgzname }) => (
	<div id="dashboard-contener" >
		<div className="row pad-tb-s70 brand-white-bg" >
			<div className="container" >
				<div className="row text-center" >
					<div className="col-md-11 col-centered" >
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
								<h4 className="" ><a href="" ><i className="fa fa-graduation-cap" ></i> Teach</a></h4>
								<p className="font-size-smaller" >Click Teach for classroom tools like presentations, handouts, and grading.</p>
							</div>
						</div>
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
							<h4 className="" ><a href="" ><i className="fa fa-calendar" ></i> Calendar</a></h4>
							<p className="font-size-smaller" >View upcoming sessions and tasks in a calendar format.</p>
						</div>
						</div>
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
							<h4 className="" ><a href="" ><i className="fa fa-gear" ></i> Setup</a></h4>
							<p className="font-size-smaller" >Work with students, classes, lessons and settings.</p>
						</div>
						</div>
					</div>
				</div>
				<div className="row text-center" >
					<div className="col-md-11 col-centered" >
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
								<h4 className="" ><a href="" ><i className="fa fa-file-text-o" ></i> Reports</a></h4>
								<p className="font-size-smaller" >See data about student and class performance and progress.</p>
							</div>
						</div>
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
							<h4 className="" ><a href="" ><i className="fa fa-search" ></i> Career Exploration</a></h4>
							<p className="font-size-smaller" >Search jobs by category and skill levels.</p>
						</div>
						</div>
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
							<h4 className="" ><a href="" ><i className="fa fa-suitcase" ></i> Portfolio Browser</a></h4>
							<p className="font-size-smaller" >View and print portfolios for the students in this class.</p>
						</div>
						</div>
					</div>
				</div>
				<div className="row text-center" >
					<div className="col-md-11 col-centered" >
						<div className="col-md-4" >
							<div className="dashobard-widget br-5 gap-s20 pad-s20" >
								<h4 className="" ><a href="" ><i className="fa fa-bullhorn" ></i> Announcements</a></h4>
								<p className="font-size-smaller" >Nothing new to say today!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
);

Dashboardcontent.propTypes = { orgzname: PropTypes.string };

export default Dashboardcontent;
