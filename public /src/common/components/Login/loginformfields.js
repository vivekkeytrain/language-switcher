import React, { PropTypes } from "react";

import classNames from "classnames";

const LoginFormFields = (
  { selfSignup, action, userid, password, onUseridChange, onPasswordChange }
) =>
  {
    // console.log('LoginForm', selfSignup, action, userid, password, onUseridChange, onPasswordChange);
    if (selfSignup && action !== "signin") {
      return (
        <div>
          <div className="form-group">
            <label htmlFor="fname" className="control-label">Name</label>
            <div>
              <div className="row">
                <div className="col-xs-6" style={{ paddingRight: "3px" }}>
                  <input
                    type="text"
                    className="form-control"
                    name="fname"
                    id="fname"
                    maxLength="32"
                    autoComplete="off"
                    required="true"
                    placeholder="First"
                  />
                </div>
                <div className="col-xs-6" style={{ paddingLeft: "3px" }}>
                  <input
                    type="text"
                    className="form-control"
                    name="lname"
                    id="lname"
                    maxLength="32"
                    autoComplete="off"
                    required="true"
                    placeholder="Last"
                  />
                </div>
              </div>
              <p className="help-block" />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="password" className="control-label">
              Create a password
            </label>
            <div className="controls">
              <input
                type="password"
                name="password"
                id="password"
                maxLength="32"
                className="form-control"
              />
              <p className="help-block" />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="email" className="control-label">Email</label>
            <div className="controls">
              <input
                type="email"
                name="email"
                id="email"
                className="form-control"
                maxLength="64"
                autoComplete="off"
                autoCapitalize="off"
                autoComplete="off"
                spellCheck="off"
              />
            </div>
            <p className="help-block" />
          </div>
        </div>
      );
    } else {
      return (
        <div className="form-group">
          <div className="controls input-group-lg">
            <label htmlFor="userid" className="control-label for-ltie9">
              Username
            </label>
            <input
              type="text"
              name="userid"
              id="userid"
              className="form-control special-userid"
              placeholder="Username"
              maxLength="64"
              autoComplete="off"
              autoCapitalize="off"
              autoCorrect="off"
              spellCheck="off"
              required="true"
              value={userid}
              onChange={onUseridChange}
            />
            <label htmlFor="school" className="control-label for-ltie9">
              Username
            </label>
            <input
              type="text"
              name="school"
              id="school"
              className="form-control special-userid"
              placeholder="school"
              maxLength="64"
              autoComplete="off"
              autoCapitalize="off"
              autoCorrect="off"
              spellCheck="off"
              required="true"
              value={userid}
              onChange={onUseridChange}
            />
            <label htmlFor="password" className="control-label for-ltie9">
              <br />Password
            </label>
            <input
              type="password"
              name="password"
              id="password"
              maxLength="32"
              className="form-control"
              placeholder="Password"
              value={password}
              onChange={onPasswordChange}
            />
          </div>
        </div>
      );
    }
  };

LoginFormFields.propTypes = {
  selfSignup: PropTypes.object,
  action: PropTypes.string.isRequired,
  userid: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onUseridChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired
};

export default LoginFormFields;
