import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import request from "superagent";

import { do_signin } from "src/common/actions";
import Notices from "src/common/components/Login/notices";
import LoginForm from "src/common/components/Login/loginform";
import Welcome from "src/common/components/Login/welcome";
import SelfSignupTop from "src/common/components/Login/selfsignuptop";

class App extends Component {
  constructor(props) {
    // console.log("Login constructor");
    super(props);

    // console.log(props);
    // const {query: {challenge}} = this.props.location;
    // let url = new URL(document.location.href, true);
    // console.log(url);
    const { query: { challenge } } = this.props.location;

    this.state = {
      userid: "",
      password: "",
      error: "",
      action: props.self_signup ? "signup" : "signin",
      notices: [],
      messages: []
    };

    this.onUseridChange = this.onUseridChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.showSignup = this.showSignup.bind(this);
    this.showSignin = this.showSignin.bind(this);
    this.changeAction = this.changeAction.bind(this);
    this.showDialog = this.showDialog.bind(this);
  }

  changeAction(action, e) {
    console.log("changeAction", action);
    e.preventDefault();
    this.setState({ action });
  }

  showSignup(e) {
    console.debug("showSignup");
    e.preventDefault();
    this.setState({ action: "signup" });
  }

  showSignin(e) {
    console.debug("showSignin");
    e.preventDefault();
    this.setState({ action: "signin" });
  }

  onUseridChange(e) {
    this.setState({ userid: e.target.value });
  }

  onPasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  handleResponse(success) {
    return (err, res) => {
      if (err || !res.ok) {
        if (res && res.body === null) {
          this.setState({ error: "Could not connect to backend." });
          return;
        }

        this.setState({ error: res.body.error });
        return;
      }

      success(res.body);
    };
  }

  showDialog(dialog_name, e) {
    let dialog = (dialog_name == 'forgot') ? this.forgotBox : this.loginBox;
    console.log("showDialog", dialog, e);
    e.preventDefault();
    let widgets = document.getElementsByClassName("widget-box");

    Array.prototype.forEach.call(widgets, widget => {
      widget.classList.remove("visible");
    });
    dialog.classList.add("visible");
  }

  onSubmit(e) {
    e.preventDefault();

    if (this.props.fetching) {
      console.log("already fetching");
      return;
    }

    console.debug(this);
    console.debug(this.state);

    if (this.props.user_data && this.props.user_data.status == 0) {
      console.log(this.props.user_data);
      //  We've already logged in and the users is moving past messages
      window.location = `${this.props.user_data.site_url}${this.props.user_data.login_session_uid}`;
    } else {
      //  Haven't yet logged in, post the login credentials
      this.props.onSignIn(this.state.userid, this.state.password).then(() => {
        console.log("done do_signin");
        console.log(this.props.user_data);

        console.log(this.props.user_data.status);

        let user_data = this.props.user_data;

        if (user_data.status == 0) {
          //  Successful state, if we don't have messages then we just redirect
          if (this.props.messages.length == 0) {
            console.log("messages");
            window.location = `${this.props.user_data.site_url}${this.props.user_data.login_session_uid}`;
          } else {
            console.log("successful login with messages");
            console.log(this.props.messages);
            this.setState({ messages: this.props.messages });
          }
        } else {
          //  Login failed, display message
          console.log('notices', this.props.notices);
          // this.setState({
          //   notices: [
          //     { type: "warning", message: "Invalid login. Please try again." }
          //   ]
          // });
          this.setState({
            notices: this.props.notices
          })
        }
      });
    }
    //  If we don't have a challenge then we've got to get one of those first
    // if (this.state.challenge == "") {}
    // request
    //   .post("/api/login")
    //   .send({
    //     username: this.state.username,
    //     password: this.state.password,
    //     challenge: this.state.challenge
    //   })
    //   .end(
    //     this.handleResponse(body => {
    //       const { challenge, authenticated } = body;
    //       console.debug("handled");
    //       console.debug(body);
    //       if (!authenticated) {
    //         this.setState({ error: "Could not authenticate." });
    //       }
    //       this.setState({
    //         authenticated: authenticated,
    //         decodedChallenge: challenge
    //       });
    //       //  assumed consent
    //       request
    //         .post("/api/consent")
    //         .send({
    //           challenge: this.state.challenge,
    //           scopes: challenge.scp,
    //           email: this.state.username
    //         })
    //         .end(
    //           this.handleResponse(({ consent }) => {
    //             window.location.href = `${this.state.decodedChallenge.redir}&consent=${consent}`;
    //             // console.debug(`${this.state.decodedChallenge.redir}&consent=${consent}`);
    //           })
    //         );
    //     })
    //   );
  }

  render() {
    // console.log("render");
    // console.log(this.props);
    // console.log(this.state);

    // let self_signup_welcome, self_signup_top, form;
    // if (this.props.self_signup) {
    //   self_signup_welcome = <div className="for-signup welcome">
    //     <span style={
    //       {
    //         fontStyle: "italic",
    //         fontFamily: "Garamond, Arial",
    //         paddingRight: "5px"
    //       }
    //     }>for</span>
    //     {this.props.self_signup.orgzname}
    //   </div>;
    // }
    // console.log("self_signup", this.props.self_signup);
    // console.log("action", this.state.action);
    // if (this.props.self_signup && this.state.action !== "signin") {
    //   self_signup_top = <div>
    //     <p>Already have an account? <a href="#" onClick={e => this.changeAction("signin", e)}>Sign in</a></p>
    //   </div>
    //   form = <div>
    //     <div className="form-group">
    //       <label htmlFor="fname" className="control-label">Name</label>
    //       <div>
    //         <div className="row">
    //           <div className="col-xs-6" style={{ paddingRight: "3px" }}>
    //             <input type="text" className="form-control" name="fname" id="fname" maxLength="32" autoComplete="off" required="true" placeholder="First" />
    //           </div>
    //           <div className="col-xs-6" style={{ paddingLeft: "3px" }}>
    //             <input type="text" className="form-control" name="lname" id="lname" maxLength="32" autoComplete="off" required="true" placeholder="Last" />
    //           </div>
    //         </div>
    //         <p className="help-block"></p>
    //       </div>
    //     </div>
    //     <div className="form-group">
    //       <label htmlFor="password" className="control-label">Create a password</label>
    //       <div className="controls">
    //         <input type="password" name="password" id="password" maxLength="32" className="form-control" />
    //         <p className="help-block"></p>
    //       </div>
    //     </div>
    //     <div className="form-group">
    //       <label htmlFor="email" className="control-label">Email</label>
    //       <div className="controls">
    //         <input type="email" name="email" id="email" className="form-control" maxLength="64" autoComplete="off" autoCapitalize="off" autoComplete="off" spellCheck="off" />
    //       </div>
    //       <p className="help-block"></p>
    //     </div>
    //   </div>;
    // } else {
    //   if (this.props.self_signup) {
    //     self_signup_top = <div>
    //         <p>Don't have an account yet? <a href="#" onClick={e => this.changeAction("signup", e)}>Sign up!</a></p>
    //       </div>
    //   }
    //   form = <div className="form-group">
    //     <div className="controls input-group-lg">
    //       <label htmlFor="userid" className="control-label for-ltie9">Username</label>
    //       <input type="text" name="userid" id="userid" className="form-control special-userid" placeholder="Username" maxLength="64" autoComplete="off" autoCapitalize="off" autoCorrect="off" spellCheck="off" required="true" value={this.state.username} onChange={this.onUsernameChange} />
    //       <label htmlFor="password" className="control-label for-ltie9"><br />Password</label>
    //       <input type="password" name="password" id="password" maxLength="32" className="form-control" placeholder="Password" value={this.state.password} onChange={this.onPasswordChange} />
    //     </div>
    //   </div>;
    // }
    let classes = classNames({ "form-box": true, [this.state.action]: true });

    return (
      <div className={classes} id="login-box">
        <div
          id="login-box-contents"
          className="widget-box visible"
          ref={d => this.loginBox = d}
        >
          <div className="header bg-navy">
            {
              this.props.site_type == 2
                ? <img
                  src="/img/ACT_CR101_logo_rev_lg.png"
                  width="330"
                  alt=""
                />
                : <img
                  src="/img/act-keytrain_rev.png"
                  width="330"
                  alt=""
                  className="img-responsive"
                />
            }
          </div>
          <LoginForm
            selfSignup={this.props.self_signup}
            action={this.state.action}
            notices={this.state.notices}
            userData={this.props.user_data}
            messages={this.state.messages}
            onSubmit={this.onSubmit}
            changeAction={this.changeAction}
            showDialog={this.showDialog}
            userid={this.state.userid}
            password={this.state.password}
            onUseridChange={this.onUseridChange}
            onPasswordChange={this.onPasswordChange}
            fetching={this.props.fetching}
          />
          {}
        </div>
        <div
          id="forgot-box"
          className="widget-box"
          ref={d => this.forgotBox = d}
        >
          <div className="header bg-navy">
            {
              this.props.site_type == 2
                ? <img
                  src="/img/ACT_CR101_logo_rev_lg.png"
                  width="330"
                  alt=""
                />
                : <img
                  src="/img/act-keytrain_rev.png"
                  alt=""
                  width="330"
                  className="img-responsive"
                />
            }
          </div>
          <div className="bg-gray" style={{ padding: "20px 10px" }}>
            <h4 className="red">Retrieve Your Password</h4>
          </div>
          <form onSubmit={this.onPasswordReset} role="form" noValidate>
            <div className="bg-gray body">
              <p>
                Provide the email address used when your account was created.
              </p>
              <div className="form-group">
                <label htmlFor="email" className="control-label">Email</label>
                <div className="controls">
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="form-control"
                  />
                  <p className="help-block" />
                </div>
              </div>
            </div>
          </form>
          <div className="footer">
            <button
              type="submit"
              id="forgot-submit-button"
              className="btn btn-primary btn-block btn-lg"
            >
              Submit
            </button>
            <div>
              <span className="for-signin inline">
                <a href="#" onClick={e => this.showDialog('login', e)}>
                  Back to login
                </a>
              </span>
              <br />
            </div>
          </div>
        </div>
        <br /><br />
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return {
    messages: state.root.messages,
    site_type: state.root.site_type,
    self_signup: state.root.self_signup,
    user_data: state.root.user_data,
    notices: state.root.notices,
    fetching: state.root.fetching
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  // console.log("mapDispatchToProps", ownProps);
  return {
    onSignIn: (userid, password) => {
      return dispatch(do_signin(userid, password));
      //  If success then we'll redirect
      //  If messages we show those on an interstitial
      //  If failure we show a message on this page
    }
  };
};

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default ConnectedApp;
