import React, { PropTypes } from "react";

import LoginFormFields from "src/common/components/Login/loginformfields";
import SelfSignupTop from "src/common/components/Login/selfsignuptop";
import Notices from "src/common/components/Login/notices";
import Welcome from "src/common/components/Login/welcome";

const LoginForm = (
  {
    selfSignup,
    action,
    notices,
    userData,
    messages,
    userid,
    password,
    fetching,
    onSubmit,
    changeAction,
    showDialog,
    onUseridChange,
    onPasswordChange
  }
) =>
  {
    if (userData && messages.length > 0) {
      //  Login was successful but there's something the user needs to know
      console.log("messages", messages);
      return (
        <div>
          <div className="body bg-gray">
            {messages.map((message, i) => (
              <div className="row" key={i}>
                <div className="col-x-12">
                  {message.msg_statdesc}
                </div>
              </div>
            ))}
          </div>
          <div className="footer">
            <button
              id="submit-button"
              className="btn btn-primary btn-block btn-lg"
              onClick={onSubmit}
            >
              {
                fetching
                  ? <i className="fa fa-spinner fa-spin fa-lg" />
                  : "Continue"
              }
            </button>
          </div>
        </div>
      );
    } else {
      //  Still need to log in
      let button_text;
      if (fetching) {
        button_text = <i className="fa fa-spinner fa-spin fa-lg" />;
      } else if (selfSignup || action !== "signin") {
        button_text = "Sign Up";
      } else {
        button_text = "Log In";
      }

      return (
        <div>
          {selfSignup ? <Welcome orgzname={selfSignup.orgzname} /> : <span />}
          <form role="form" noValidate onSubmit={onSubmit}>
            <div className="body bg-gray">
              <SelfSignupTop
                selfSignup={selfSignup}
                action={action}
                changeAction={changeAction}
              />
              <Notices notices={notices} />
              <LoginFormFields
                selfSignup={selfSignup}
                action={action}
                userid={userid}
                password={password}
                onUseridChange={onUseridChange}
                onPasswordChange={onPasswordChange}
              />
            </div>
            <div className="footer">
              <button
                id="submit-button"
                type="submit"
                className="btn btn-primary btn-block btn-lg"
              >
                {button_text}
              </button>
              <div>
                <span className="for-signin inline">
                  <a href="#" onClick={e => showDialog('forgot', e)}>
                    I forgot my password
                  </a>
                </span>
                <span className="for-cr101 inline pull-right">
                  <a href="http://www.keytrain.com/CareerReady101">
                    What is Career Ready 101?
                  </a>
                </span>
                <span className="for-keytrain inline pull-right">
                  <a href="http://www.keytrain.com/">What is KeyTrain?</a>
                </span>
                <br />
              </div>
            </div>
          </form>
        </div>
      );
    }
  };

LoginForm.PropTypes = {
  selfSignup: PropTypes.object,
  action: PropTypes.string.isRequired,
  notices: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      message: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  userData: PropTypes.object,
  messages: PropTypes.array,
  onSubmit: PropTypes.func.isRequired,
  changeAction: PropTypes.func.isRequired,
  showDialog: PropTypes.func.isRequired,
  userid: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  onUseridChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  fetching: PropTypes.bool.isRequired
};

export default LoginForm;
