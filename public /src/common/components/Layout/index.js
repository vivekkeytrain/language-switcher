import React from "react";

export default class extends React.Component {
  swapBackground(new_class, e) {
    console.log("swapBackground");
    e.preventDefault();
    console.log(e);
    console.log("swapBackground", new_class);
    console.log(this.state);
    console.log("set class");
    document.getElementsByTagName(
      "body"
    )[0].className = `login-layout ${new_class}`;
  }

  render() {
    // console.log('Layout render');
    return (
      <div id="wrapper" >
        <div id="content">
          {this.props.children}
        </div>
        <div id="footer" className="small bg-gray footer-hidden">
          <div className="container">
            <ul className="list-inline hidden-print">
              <li>
                <span id="copyright">
                  © {new Date().getFullYear()} by ACT, Inc.
                </span>
              </li>
              <li>
                <a href="http://www.act.org/disclaimer.html" target="_blank">
                  Terms of Use
                </a>
              </li>
              <li>
                <a href="http://www.act.org/privacy.html" target="_blank">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
