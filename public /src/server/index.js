import express from "express";
import path from "path";
import React from "react";
import consolidate from "consolidate";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import { match, RouterContext } from "react-router";
import bodyParser from "body-parser";
// import flash from "connect-flash";
// import session from "express-session";

import handlers from "src/server/handler";
import configureStore from "src/common/store";
import routes from "src/common/routes";
// import { SESSION_SECRET, SESSION_EXPIRATION } from "src/common/config";

const port = process.env.PORT || 3000;
const app = express();
const root = path.join(process.cwd(), "public");

export const isProduction = env =>
  (env || process.env).NODE_ENV === "production";

if (!isProduction()) {
  const webpack = require("webpack");
  const webpackConfig = require("webpack.config.client");
  const compiler = webpack(webpackConfig);

  app.use(
    require("webpack-dev-middleware")(compiler, {
      noInfo: true,                
      publicPath: webpackConfig.output.publicPath
    })
  );
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(flash());

// var sess = {
//   secret: SESSION_SECRET,
//   cookie: { _expires: SESSION_EXPIRATION },
//   resave: false,
//   saveUninitialized: false
// };

// if (isProduction()) {
//   sess.cookie.secure = true; // serve secure cookies
// }

// app.use(session(sess));

app.use(express.static(root));
handlers.bindAll(app);

// app.get('/', (req, res, next) => {
// })
// app.post('/api/login', (req, res, next) => {
//     console.log(req);
//     res.send('done');
// })

// const self_signup = { orgzname: "Coptix" };
const self_signup = null;
app.use((req, res) => {
  console.log('HOST', req.get('HOST'));
  const site_type = req.get('HOST').indexOf('keytrain') !== -1 ? 1 : 2;
  
  const initialState = {
    root: {
      messages: [],
      site_type: site_type,
      self_signup: 'self_signup'
    }
  };

  // console.log(initialState);
  const store = configureStore(initialState);
  // console.log(store)
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) =>
    {
      if (error) {
        res.status(500);
        res.send(error.message);
      } else if (redirectLocation) {
        res.redirect(302, redirectLocation.pathname + redirectLocation.search);
      } else if (renderProps) {
        const renderedContent = renderToString(
          <Provider store={store}>
            <RouterContext {...renderProps} />
          </Provider>
        );

        consolidate.handlebars(
          "src/client/index.handlebars",
          {
            html: renderedContent,
            state: JSON.stringify(initialState),
            keytrain_site: site_type == 1 ? true : false,
            self_signup: self_signup
          },
          (error, html) => {
            if (error) {
              console.error(error);
              throw error;
            }

            res.status(200);
            res.send(html);
          }
        );
      }
    });
});

app.listen(port, () => {
  console.log(
    `Listening on port ${port}. Open up http://localhost:${port}/ in your browser`
  );
});
