import request from "request";

export default app => {
  app.post("/api/login", (req, res) => {
    console.log("/api/login");
    const { username, password } = req.body;

    // http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/json?userid=griselda&password=sdf&pkg_id=2&login_domain=dev.login.careerready101.com
    console.log(
      `http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/json?userid=${username}&password=${password}&pkg_id=2&login_domain=dev.login.careerready101.com`
    );

    request(
      `http://sdapi.actkeytrain.com/dbwebapi/dbo.api_sp_login_working/?userid=${username}&password=${password}&pkg_id=2&login_domain=dev.login.careerready101.com`,
      (err, resp, body) => {
        let result = JSON.parse(body);

        if (
          !result.hasOwnProperty("ResultSets") ||
            result.ResultSets.length < 1 ||
            result.ResultSets[0].length < 1
        ) {
          //  Something wrong with the json, return an error
          res.status(500).send("API Error");
        }

        console.log(result.ResultSets);
        res.json(result.ResultSets);
        // if (resultset.status == 0) {
        //     res.redirect(`${resultset.site_url}${resultset.login_session_uid}`);
        // } else {
        //     res.json({
        //         status: resultset.status,
        //         statdesc: resultset.statdesc
        //     })
        // }
      }
    );
  });
};
